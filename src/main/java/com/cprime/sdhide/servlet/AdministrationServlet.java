package com.cprime.sdhide.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.templaterenderer.TemplateRenderer;

public class AdministrationServlet extends HttpServlet{
	public String GENERAL_UI_TEMPLATE="/templates/admin.vm";
	private final TemplateRenderer templateRenderer;
	private final PluginSettingsFactory pluginSettingsFactory;
	private static final String PLUGIN_STORAGE_KEY = "com.cprime.plugin.sdhide.issueType.";
	public AdministrationServlet(TemplateRenderer templateRenderer, PluginSettingsFactory pluginSettingsFactory) {
		  this.templateRenderer = templateRenderer;
		  this.pluginSettingsFactory=pluginSettingsFactory;
	}
	
    private static final Logger log = LoggerFactory.getLogger(AdministrationServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
    	UserManager userManager = ComponentAccessor.getUserManager();
        Set<Group> groups = userManager.getAllGroups();
        Collection<IssueType> issueTypes=ComponentAccessor.getConstantsManager().getAllIssueTypeObjects();
        Map<String, Object> context = new HashMap<String, Object>();
        
        Map<String, String> userChoice=new HashMap<String, String>();
        
        PluginSettings pluginSettings = pluginSettingsFactory.createGlobalSettings();
        Iterator<IssueType> iterIssue=issueTypes.iterator();
        while(iterIssue.hasNext()){
        	IssueType issueType=iterIssue.next();
        	if(pluginSettings.get(PLUGIN_STORAGE_KEY+issueType.getName())!=null || !pluginSettings.get(PLUGIN_STORAGE_KEY+issueType.getName()).equals(""))
        		userChoice.put(issueType.getName(), (String) pluginSettings.get(PLUGIN_STORAGE_KEY+issueType.getName()));
        	else{
        		userChoice.put(issueType.getName(), "");
        	}
        }
        context.put("userChoice", userChoice);
        context.put("groups", groups);
        context.put("issueTypes", issueTypes);
        templateRenderer.render(GENERAL_UI_TEMPLATE, context, resp.getWriter());
    }
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
    	System.out.println("entered post...");
    	Collection<IssueType> issueTypes=ComponentAccessor.getConstantsManager().getAllIssueTypeObjects();
        Iterator<IssueType> iterIssue=issueTypes.iterator();
        PluginSettings pluginSettings = pluginSettingsFactory.createGlobalSettings();
        ArrayList<String> list=new ArrayList<String>();
        while(iterIssue.hasNext()){
        	IssueType issueType=iterIssue.next();
        	String userChoiceStr="";
        	String[] values=req.getParameterValues(issueType.getName());
        	if(values!=null){
	        	for(int i=0; i<values.length;i++){
	        		userChoiceStr=userChoiceStr+","+values[i];
	        	}
        	}
        	if(!userChoiceStr.equals("")){
        		pluginSettings.put(PLUGIN_STORAGE_KEY+issueType.getName(), userChoiceStr);
        		System.out.println(PLUGIN_STORAGE_KEY+issueType.getName()+" : "+values);
        	}
        }
        Map<String, String> userChoice=new HashMap<String, String>();
        Iterator<IssueType> iterIssue2=issueTypes.iterator();
        while(iterIssue2.hasNext()){
        	IssueType issueType=iterIssue2.next();
        	userChoice.put(issueType.getName(), (String)pluginSettings.get(PLUGIN_STORAGE_KEY+issueType.getName()));
        }
        System.out.println(userChoice);
        UserManager userManager = ComponentAccessor.getUserManager();
        Set<Group> groups = userManager.getAllGroups();
        Map<String, Object> context = new HashMap<String, Object>();
        context.put("userChoice", userChoice);
        context.put("groups", groups);
        context.put("issueTypes", issueTypes);
        templateRenderer.render(GENERAL_UI_TEMPLATE, context, resp.getWriter());
    }
}