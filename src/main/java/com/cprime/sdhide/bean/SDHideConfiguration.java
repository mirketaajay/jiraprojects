package com.cprime.sdhide.bean;

public class SDHideConfiguration {
	private String IssueType;
	private String userGroup;
	public String getIssueType() {
		return IssueType;
	}
	public void setIssueType(String issueType) {
		IssueType = issueType;
	}
	public String getUserGroup() {
		return userGroup;
	}
	public void setUserGroup(String userGroup) {
		this.userGroup = userGroup;
	}
}
